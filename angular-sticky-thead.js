angular.module('angularStickyThead', [])
  .directive('stickyThead', function($timeout) {
  return {
    link: function(scope, el) {
      var selectors = {
        tableClone: '#sticky-thead',
        theadClone: '#sticky-thead > thead'
      };
      scope.windowHorizontalScroll = 0;
      scope.wrapperHorizontalScroll = 0;

      $timeout(function() {
        scope.tableOffset = el.offset().top;
        // create a copy of the table and header
        var $table = el.clone(true);
        $table.attr('id', 'sticky-thead');
        $table.find('tbody').each(function() {
          $(this).remove();
        });
        $($table).insertAfter(el);
        scope.$fixedHeader = $(selectors.tableClone);
        scope.adjustThead(this);
      });

//-- LISTENERS --

      $(window).scroll(function() {
        scope.adjustThead(this);
      });

      $(window).resize(function() {
        scope.adjustHeaderWidth(el);
      });

      el.parent().scroll(function() {
        scope.wrapperHorizontalScroll = $(this).scrollLeft();
        $(selectors.theadClone).css('left', scope.overallHorizontalScroll);
      });

//-- HELPERS --

      scope.adjustThead = function(element) {
        scope.windowHorizontalScrollAdjust(element);
        scope.windowVerticalScrollAdjust(element);
        scope.adjustHeaderWidth(el);
      };

      scope.windowHorizontalScrollAdjust = function(element) {
        scope.windowHorizontalScroll = $(element).scrollLeft();
        $(selectors.theadClone).css('left', scope.overallHorizontalScroll);
      };

      scope.windowVerticalScrollAdjust = function(element) {
        var offset = $(element).scrollTop();
        if (offset >= scope.tableOffset && scope.$fixedHeader.is(':hidden')) {
          scope.adjustHeaderWidth(el);
          scope.$fixedHeader.css('display', 'block');
        }
        else if (offset < scope.tableOffset) {
          scope.$fixedHeader.hide();
        }
      };

      scope.adjustHeaderWidth = function(el) {
        $(selectors.theadClone).width(el.width());
        el.find('th').each(function(index) {
          index = index + 1;
          var originalWidth = el.find('th:nth-child(' + index + ')').width();
          $(selectors.tableClone + ' th:nth-child(' + index + ')').width(originalWidth);

        });
      };

      scope.overallHorizontalScroll = function() {
        return -Math.abs(scope.windowHorizontalScroll + scope.wrapperHorizontalScroll);
      };
    }
  };
});
