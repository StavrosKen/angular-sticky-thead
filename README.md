# angular-sticky-thead 

> Angular plugin for always visible table headers

## Features

* Makes any table's headers visible when you scroll
* Keeps headers functional with their original styling and events
* 100% Responsive
* Compatible with horizontal scrolling

## Installation

- Run: `bower install --save angular-sticky-thead`
- Include the appropriate files in your index.html
```html
<link rel="stylesheet" type="text/css" href="<path-to-angular-sticky-thead>/angular-sticky-thead.css">
<script src="<path-to-angular-sticky-thead>/angular-sticky-thead.js"></script>
```
- Include `angularStickyThead` in your module
```javascript
angular.module('myApp', ['angularStickyThead']);
```

## Usage

- Add the `sticky-thead` attribute in any table element
```html
<table sticky-thead>
...
</table>
```


## How it works

1. Creates a clone of your table's header
2. Hides the clone
3. Sets the appropriate size on every element on the table (comparing the <th> with the corresponding <td>s and setting the width to the widest)
4. When the user scrolls, shows the cloned `<th>`s if the original `<th>`s are off-screen
5. When the window is resized or the window is scrolled horizontally, resizes the `<th>`s accordingly

## License

[MIT license](http://opensource.org/licenses/mit-license.php)